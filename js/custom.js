jQuery(document).ready(function ($) {
    'use strict';
    function breakAroundSpace(str) {
        var parts = [];
        for (var match; match = str.match(/^[\s\S]{1,200}\S*/);) {
            var prefix = match[0];
            parts.push(prefix);
            // Strip leading space.
            str = str.substring(prefix.length).replace(/^\s+/, '');
        }
        if (str) { parts.push(str); }
        return parts;
    }
    function itemsRecogidos(items,position) {
        var html = '';
        var i = 1;
        $.each( items, function( key, value ) {
            if (i==1 || i ==4 || i ==7 || i ==10 || i ==13){
                html += '<div class="row">';
            }
            var temp = items[key]["description"];
            var temp2 = temp.split("Contenido:");
            var textofinal ="";
            var image = "";
            if (temp2.length>1){

                var tempimg = temp2[1].split('src="//');
                var img = "";
                if (tempimg.length>1){
                    var tempimg2 = tempimg[1].split('"');
                    img = tempimg2[0];
                }
                var temp3 = $.parseHTML( temp2[1] );
                var text = $(temp3).text();
                text = text.trim();
                var temp4 = breakAroundSpace(text);
                textofinal = temp4[0];

                if (img!=""){
                    image = '<div><img src="https://'+img+'" alt="TUS Santander" /></div>';
                }
            }
            html += '<div class="col-md-4">' +
                '<div class="views-field-title"><a href="'+items[key]["link"]+'" target="_blank">'+items[key]["title"]+'</a></div>' +
                '<div class="views-field-field-fecha-del-articulo">'+moment(items[key]["pubDate"]).format('L')+'</div>' +
                '<div>'+textofinal+'...</div>' + image +
                '</div>'
            if (i ==3 || i ==6 || i ==9 || i ==12){
                html += '</div>'
            }
            i++;
        });
        if(position==1){
            $('#block-noticiasdeportada').append(html);
        }
        if(position==2){
            $('#block-acercadetusnoticias').append(html);
        }
    }
    if ($('#block-noticiasdeportada').length) {
        var items = new Array();
        $.get("https://www.santander.es/tus/actualidad", function(data) {
            var $xml = $(data);

            $xml.find("item").each(function() {
                var $this = $(this),
                    item = {
                        link: $this.find("link").text(),
                        title: $this.find("title").text(),
                        pubDate: $this.find("pubDate").text(),
                        description: $this.find("description").text()
                    };
                items.push(item);
            });
            itemsRecogidos(items,1);
        });
    }
    if ($('#block-acercadetusnoticias').length) {
        var items = new Array();
        $.get("https://www.santander.es/tus/noticias", function(data) {
            var $xml = $(data);

            $xml.find("item").each(function() {
                var $this = $(this),
                    item = {
                        link: $this.find("link").text(),
                        title: $this.find("title").text(),
                        pubDate: $this.find("pubDate").text(),
                        description: $this.find("description").text()
                    };
                items.push(item);
            });
            itemsRecogidos(items,2);
        });
    }
});
